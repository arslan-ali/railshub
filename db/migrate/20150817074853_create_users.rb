class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
			t.string :first_name, default: ""
			t.string :last_name, default: ""
			t.string :username, default: ""

			t.string :twitter_link, default: ""
			t.string :website_link, default: ""
			t.string :github_link, defaut: ""

			t.string :email, default: ""
			t.boolean :email_status, default: false

			t.text :about_me

			t.string :location
			t.integer :total_visited_days

			t.date :birth

      t.timestamps null: false
    end
  end
end
