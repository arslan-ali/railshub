class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.references :user, index: true
			t.string :name
      t.string :description

      t.timestamps null: false
    end
    add_foreign_key :tags, :users
  end
end
