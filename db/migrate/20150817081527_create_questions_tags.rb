class CreateQuestionsTags < ActiveRecord::Migration
  def change
    create_table :questions_tags, id: false do |t|
      t.references :question, index: true
      t.references :tag, index: true
    end
    add_foreign_key :question_tags, :questions
    add_foreign_key :question_tags, :tags
  end
end
