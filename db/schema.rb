# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150817081527) do

  create_table "answers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.text     "answer"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id"
  add_index "answers", ["user_id"], name: "index_answers_on_user_id"

  create_table "questions", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "question"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "questions", ["user_id"], name: "index_questions_on_user_id"

  create_table "questions_tags", id: false, force: :cascade do |t|
    t.integer "question_id"
    t.integer "tag_id"
  end

  add_index "questions_tags", ["question_id"], name: "index_questions_tags_on_question_id"
  add_index "questions_tags", ["tag_id"], name: "index_questions_tags_on_tag_id"

  create_table "tags", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "tags", ["user_id"], name: "index_tags_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name",         default: ""
    t.string   "last_name",          default: ""
    t.string   "username",           default: ""
    t.string   "twitter_link",       default: ""
    t.string   "website_link",       default: ""
    t.string   "github_link"
    t.string   "email",              default: ""
    t.boolean  "email_status",       default: false
    t.text     "about_me"
    t.string   "location"
    t.integer  "total_visited_days"
    t.date     "birth"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

end
