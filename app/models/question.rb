class Question < ActiveRecord::Base

	validates :question, :user_id, presence: true

	has_many :answers
	belongs_to :user
	has_and_belongs_to_many :tags
end
