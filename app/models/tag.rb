class Tag < ActiveRecord::Base
	validates :user_id, presence: true
	validates :name, presence: true, uniqueness: true

	belongs_to :user
	has_and_belongs_to_many :questions
end
