class Answer < ActiveRecord::Base

	validates :answer, :user_id, presence: true

	belongs_to :user
	belongs_to :question
end
