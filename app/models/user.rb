class User < ActiveRecord::Base

	validates :first_name, :last_name, presence: true
	validates :username, presence: true, format: {
		                                     with: /\A[A-Za-z\d][A-Za-z\d_]+\z/,
																				 message: "should be formatted properly"
																			 }, length: { maximum: 40, minimum: 3 },
																			 uniqueness: true
	validates :location, presence: true
	validates :email, presence: true, format: {
																			with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i,
																			message: "should be formatted properly"
																		}


	has_many :questions
	has_many :tags
	has_many :answers
	
end
