require 'rails_helper'

RSpec.describe Tag, type: :model do
  # Validation tests
	it { should validate_presence_of(:name) }

	# Association tests
	it { should belong_to(:user) }
	it { should have_and_belong_to_many(:questions) }
end
