require 'rails_helper'

RSpec.describe Question, type: :model do

  # Validation tests
	it { should validate_presence_of(:question) }

	# Association tests
	it { should have_many(:answers) }
	it { should belong_to(:user) }
	it { should have_and_belong_to_many(:tags) }
end
