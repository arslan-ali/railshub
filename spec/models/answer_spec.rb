require 'rails_helper'

RSpec.describe Answer, type: :model do
  # Validation tests
	it { should validate_presence_of(:answer) }

	# Association tests
	it { should belong_to(:question) }
	it { should belong_to(:user) }
end
