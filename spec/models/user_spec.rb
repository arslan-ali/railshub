require 'rails_helper'

RSpec.describe User, type: :model do

	context "Without anything" do
		it { should_not be_valid }
	end

	
  # Validation tests
	it { should validate_presence_of(:first_name) }
	it { should validate_presence_of(:last_name) }
	it { should validate_presence_of(:username) }
	it { should validate_presence_of(:location) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:username) }
	it { should validate_length_of(:username).is_at_least(3).is_at_most(40) }
	it { should allow_value("abc123").for(:username) }
	it { should allow_value("abcabc").for(:username) }
	it { should allow_value("abc_abc").for(:username) }
	it { should allow_value("123abc").for(:username) }
	it { should_not allow_value("_123").for(:username) }
	it { should_not allow_value("$_123").for(:username) }
	it { should_not allow_value(" _123").for(:username) }
  it { should allow_value("test@test.com").for(:email) }
	it { should_not allow_value("123@132").for(:email) }

	# Association tests
	it { should have_many(:questions) }
	it { should have_many(:answers) }
	it { should have_many(:tags) }
end

