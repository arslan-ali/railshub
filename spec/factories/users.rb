FactoryGirl.define do
  factory :user do
    first_name "John"
		last_name "Don"
		sequence(:username) { |i| "test#{i}" }
		birth { 10.years.from_now }
		twitter_link "http://twitter.com/test"
		about_me "This time, I'm just a test user"
		location "Lahore, Pakistan"
		website_link "http://simplesite.com"
		total_visited_days 40
		github_link "http://github.com/test"
		sequence(:email) { |i| "test#{i}@test.com" }
		email_status false
  end

end
