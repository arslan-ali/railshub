FactoryGirl.define do
  factory :tag do
		sequence(:name) { |i| "test#{i}"}
		description "Very simple description"
		association :user
  end

end
